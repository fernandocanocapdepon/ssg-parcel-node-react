const express = require('express');
const cors = require('cors');
const app = express();
const bd = require('./bd');
const { products } = require('./bd');
app.use(cors());
app.use(express.json());

const PORT = 6002;
const HOST = "localhost";

app.get('/ws-product/:id', (req, res) => {
  return res.send(bd.products.find((pro) => pro.productId == parseInt(req.params.id)));
})

app.get('/ws-catalog/:label', (req, res) => {
  const catalog = bd.catalogs.find((catalog) => req.params.label === catalog.label);
  return res.send({
    label: req.params.label,
    products: catalog.list.map((id) => products.find((product) => product.productId === id)),
  });
});

app.get('/ws-catalog', (req, res) => {
  return res.send({
    label: req.params.label,
    products: bd.products,
  });
});

app.get('/ws-menu', (req, res) => {
  return res.send([
    { label: 'HOME', href: '/' },
    { label: 'Men', href: '/catalog/men' },
    { label: 'Woman', href: '/catalog/woman' },
    { label: 'Search', href: '/search' }
  ]);
});

app.post('/ws-labels', (req, res) => {
  const labels = {
    "header": { "es": "Hello! Im the header" },
    "catalog": { "es": "Hello! Im the catalog" },
    "footer": { "es": "Hello! Im the footer" },
    "home.title": { "es": "Titulo Home", "en": "Home title", "ar": "Home AR" },
    "footer": { "es": "Hello! Im the footer" },
    "never": { "es": ":(" }
  }
  const responseLabels = {};
  Object.keys(labels).forEach((key) => {
    if (req.body.keys.indexOf(key) > -1) {
      responseLabels[key] = labels[key];
    }
  })
  return res.send(responseLabels);
});

app.listen(PORT, HOST, () => {
  console.log(`Starting Proxy at ${HOST}:${PORT}`);
});