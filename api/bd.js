module.exports = {
  catalogs: [
    {
      label: 'men',
      list: [1, 2, 3, 4, 5, 8, 12]
    },
    {
      label: 'woman',
      list: [6, 7, 9, 10, 11]
    },
  ],
  products: [
    {
      productId: 1,
      name: 'Top punto calado',
      price: '25,99 €',
      imageUrl: 'http://localhost:8000/assets/blue.png',
    },
    {
      productId: 2,
      name: 'Chaleco punto combinado',
      price: '29,99 €',
      imageUrl: 'http://localhost:8000/assets/orange.png',
    },
    {
      productId: 3,
      name: 'Chaleco crochet',
      price: '29,99 €',
      imageUrl: 'http://localhost:8000/assets/white.png',
    },
    {
      productId: 4,
      name: 'Jersey crochet crop',
      price: '39,99 €',
      imageUrl: 'http://localhost:8000/assets/orange.png',
    },
    {
      productId: 5,
      name: 'Top punto calado',
      price: '25,99 €',
      imageUrl: 'http://localhost:8000/assets/red.png',
    },
    {
      productId: 6,
      name: 'Chaleco punto combinado',
      price: '29,99 €',
      imageUrl: 'http://localhost:8000/assets/orange.png',
    },
    {
      productId: 7,
      name: 'Chaleco crochet',
      price: '29,99 €',
      imageUrl: 'http://localhost:8000/assets/red.png',
    },
    {
      productId: 8,
      name: 'Chaleco crochet',
      price: '29,99 €',
      imageUrl: 'http://localhost:8000/assets/white.png',
    },
    {
      productId: 9,
      name: 'Jersey crochet crop',
      price: '3,99 €',
      imageUrl: 'http://localhost:8000/assets/white.png',
    },
    {
      productId: 10,
      name: 'Jersey crochet crop',
      price: '9,99 €',
      imageUrl: 'http://localhost:8000/assets/red.png',
    },
    {
      productId: 11,
      name: 'Jersey crochet crop',
      price: '29,99 €',
      imageUrl: 'http://localhost:8000/assets/blue.png',
    },
    {
      productId: 12,
      name: 'Jersey crochet crop',
      price: '39,99 €',
      imageUrl: 'http://localhost:8000/assets/blue.png',
    },
  ]
}