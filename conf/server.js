const express = require('express');
const cors = require('cors');
// const simulateLatency = require('express-simulate-latency');

// Create Express Server
// const smallLag = simulateLatency({ min: 700, max: 1200 });
const app = express();
app.use(cors());
//app.use(smallLag);

// Configuration
const PORT = 6003;
const HOST = "localhost";

app.use(express.static('conf/'));

// Start the Proxy
app.listen(PORT, HOST, () => {
  console.log(`Starting Proxy at ${HOST}:${PORT}`);
});