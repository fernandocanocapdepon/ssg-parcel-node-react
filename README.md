## Manguitos

## Set up project

Core dependencies
```sh
yarn
```

External packages (React micro-frontends)
```sh
cd external
```

```sh
yarn
```

## 1. Start servers

Root folder
```sh
yarn server
```

## 2. Build packages
External packages (React micro-frontends)
```sh
cd external
```

```sh
yarn build
```

## 2. Generate templates
Root folder
```sh
yarn build
```

## 2. Show result web site sever generated
```http
http://localhost
```