const express = require('express');
const path = require('path');
const fs = require('fs');
const cors = require('cors');
const { createProxyMiddleware } = require('http-proxy-middleware');

// Create Express Server
const app = express();
app.use(cors())

app.use('/ws-*', createProxyMiddleware({ target: 'http://localhost:6002', changeOrigin: true }));
app.use('/st-*', createProxyMiddleware({ target: 'http://localhost:6001', changeOrigin: true }));
app.use('/assets/*', createProxyMiddleware({ target: 'http://localhost:6001', changeOrigin: true }));

// Configuration
const PORT = 8000;
const HOST = "localhost";

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '../www/es/home.html'));
});

app.get('/home', function (req, res) {
  res.sendFile(path.join(__dirname, '../www/es/home.html'));
});

app.get('/catalog/:label', function (req, res) {
  if (fs.existsSync(path.join(__dirname, `../www/es/catalog.${req.params.label}.html`))) {
    res.sendFile(path.join(__dirname, `../www/es/catalog.${req.params.label}.html`));
  } else {
    res.sendFile(path.join(__dirname, `../www/es/catalog.html`));
  }
});

app.get('/search', function (req, res) {
  res.sendFile(path.join(__dirname, `../www/es/catalog.html`));
});

app.get('/product/:id', function (req, res) {
  if (fs.existsSync(path.join(__dirname, `../www/es/product.${req.params.id}.html`))) {
    res.sendFile(path.join(__dirname, `../www/es/product.${req.params.id}.html`));
  } else {
    res.sendFile(path.join(__dirname, `../www/es/product.html`));
  }
});

app.use(express.static('www'));

// Start the Proxy
app.listen(PORT, HOST, () => {
  console.log(`Starting Proxy at ${HOST}:${PORT}`);
});