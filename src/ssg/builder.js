import Parser from './parser';
import SSR from './ssr';
import { htmlTemplate, saveHtml, localeFolder } from './helpers';

class Builder {
  /**
   * Get list of rendered components from micro-fronted compiled codes
   * @param {Object} config build configuration 
   * @param {Object} packagesCode micro-fronted compiled codes
   * @returns Promise<Array<Object>>
   */
  async getRendered(config, packagesCode) {
    return Promise.all(config.packages.map(async (packageEntry) => {
      return {
        name: packageEntry.name,
        rendered: await SSR(
          packageEntry.host,
          packageEntry.name,
          packageEntry.params,
          config.dir,
          packagesCode,
        ),
      }
    }));
  }
  /**
   * 
   * @param {Array<Objects>} packages 
   * @param {string} html
   * @param {Array<Object>} renders
   * @returns 
   */
  replaceInHtml(packages, html, renderedList) {
    let replacedHtml = html;
    packages.forEach((packageEntry) => {
      console.log(`\x1b[36m[package] ${packageEntry.name} ${packageEntry.params ? JSON.stringify(packageEntry.params) : ''}\x1b[0m`);
      const renderedPackage = renderedList
        .find((renderedItem) => renderedItem.name === packageEntry.name)
        .rendered;
      const regexp = new RegExp(`<div[ a-zA-Z0-9"-=]*package-def="${packageEntry.name}"[ a-zA-Z0-9"-=]*><\/div>`);
      replacedHtml = replacedHtml.replace(
        regexp,
        renderedPackage,
      );
    });
    return replacedHtml;
  }
  /**
   * 
   * @param {Object} config 
   * @param {string} locale 
   * @param {Object} translations 
   * @param {Object} packagesCode 
   */
  async entry(config, locale, translations, packagesCode) {
    // Create local folder if no exits
    localeFolder(locale);
    let html = htmlTemplate(config.template);
    const outFile = config.out || config.template;
    const lang = locale.split('-')[0];
    let parsed = await Parser(html, lang, config.dir, translations);
    const renderedList = await this.getRendered(config, packagesCode);
    console.log(`\x1b[32m[from] ${config.template}\x1b[0m`);
    console.log(`\x1b[32m[locale] ${locale}\x1b[0m`);
    parsed = this.replaceInHtml(config.packages, parsed, renderedList)
    console.log(`\x1b[35m[to] ${locale}/${outFile}\x1b[0m`);
    await saveHtml(parsed, outFile, locale);
  }
}

export default Builder;