import fs from 'fs';
import path from 'path';
import fetch from 'node-fetch';

const DIST_PATH = '../../www/'; 
/**
 * Get list of unique elements as a filter param
 * @param {string} value
 * @param {string} index
 * @param {string} self
 * @returns 
 */
export const unique = (value, index, self) => self.indexOf(value) === index;
/**
 * GEt html template
 * @param {string} template
 * @returns 
 */
export const htmlTemplate = (template) => fs.readFileSync(path.resolve(__dirname, `${DIST_PATH}${template}`), "utf8");
/**
 * Save a generated final html
 * @param {string} html
 * @param {string} template
 * @param {string} locale
 */
export const saveHtml = (html, template, locale) => {
  fs.writeFileSync(path.resolve(__dirname, `${DIST_PATH}${locale}/${template}`), html, "utf8");
};
/**
 * Create a locale folder if not exists
 * @param {string} locale 
 */
export const localeFolder = (locale) => {
  if (!fs.existsSync(path.resolve(__dirname, `${DIST_PATH}${locale}`))) {
    fs.mkdirSync(path.resolve(__dirname, `${DIST_PATH}${locale}`));
  }
}
/**
 * Get source code from url
 * @param {string} url 
 * @returns 
 */
export const getSourceCodeFromUrl = (url) => new Promise((resolve) => {
  fetch(url)
    .then(res => res.text())
    .then(async (code) => {
      resolve(code);
    });
});

/**
 * Get manifest json form url
 * @param {string} url 
 * @returns 
 */
export const getManifest = (host) => new Promise((resolve) => {
  fetch(`${host}manifest.json`)
    .then(res => res.json())
    .then(async (manifest) => {
      resolve(manifest);
    });
});