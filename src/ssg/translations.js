import fetch from 'node-fetch';
import { unique, htmlTemplate } from './helpers';
class Translations {
  constructor(host, locales) {
    this.RESERVED_KEYS = ['lang', 'dir', 'locale'];
    this.translationsServiceUrl = `${host}/ws-labels?idiomas=${locales.join(',')}`;
  }
  /**
   * Extract translations keys from a html template
   * @param {string} template
   * @returns 
   */
  keysFromTemplate(template) {
    const noReservedKey = (key) => this.RESERVED_KEYS.indexOf(key) < 0;
    const html = htmlTemplate(template);
    return html.match(/{{([a-zA-Z0-1\.]+)}}/gm).map((key) => /{{([a-zA-Z0-1\.]+)}}/.exec(key)[1]).filter(noReservedKey);
  }
  /**
   * Get all translations from a list of html templates
   * @param {string} templates 
   * @returns 
   */
  async translationsFromTemplates(templates) {
    let keys = [];
    templates.forEach((template) => {
      keys = [...keys, ...this.keysFromTemplate(template)].filter(unique);
    });
    const translations = await fetch(this.translationsServiceUrl, {
      method: 'POST',
      body: JSON.stringify({ keys }),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((res) => res.json());
    return translations;
  }
}
export default Translations;
