import fetch from 'node-fetch';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { getManifest, getSourceCodeFromUrl } from './helpers';


const SSR = async (host, name, params = {}, dir = 'ltr', microsCodes) => {
  const manifest = await getManifest(host);
  const compiled = microsCodes[name];
  const statics = await compiled.getStatics(fetch, params);
  const Component = compiled.default;
  const render = ReactDOMServer.renderToString(<Component {...statics} />);
  const css = await getSourceCodeFromUrl(`${host}${manifest[dir === 'ltr' ? 'styles.css' : 'styles.rtl.css']}`);
  const hydrated = await getSourceCodeFromUrl(`${host}${manifest['hydrate.js']}`);
  const out = `
        <script>window.__hydrated = window.__hydrated || []; window.__hydrated['${name}'] = ${JSON.stringify({ ...statics })};</script>
        <div hydrate="${name}">${render}</div>
        <script>${hydrated}</script>
        <style>${css}</style>
      `;
  return out;
};

export default SSR;

