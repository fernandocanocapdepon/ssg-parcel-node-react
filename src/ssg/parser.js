const Parser = async (html, locale, dir = 'ltr', translations) => {
  const lang = locale.split('-')[0];
  let finalHtml = html;
  finalHtml = finalHtml.replace(
    new RegExp('{{locale}}'),
    locale,
  );
  finalHtml = finalHtml.replace(
    new RegExp('{{lang}}'),
    lang,
  );
  finalHtml = finalHtml.replace(
    new RegExp('{{dir}}'),
    dir,
  );
  Object.keys(translations).forEach((key) => {
    const regex = `{{${key}}}`;
    finalHtml = finalHtml.replace(
      new RegExp(regex),
      translations[key][lang],
    );
  })
  return finalHtml;
}

export default Parser;