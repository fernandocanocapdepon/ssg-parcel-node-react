
import vm from 'vm';
import React from 'react';
import { getSourceCodeFromUrl } from './helpers';
class Codes {
  /**
   * Get all packages that main list not contains yet
   * @param {Array<Objects>} packagesList
   * @param {Array<Objects>} mainPackagesList
   * @returns 
   */
  getUniquePackages(packagesList, mainPackagesList) {
    return packagesList.filter((item) => !mainPackagesList
      .find((code) => item.name === code.name))
      .map((code) => ({ name: code.name, host: code.host }))

  }
  /**
   * Obtains all compiled source code of all packages in the config
   * @param {Array<Object>} config 
   * @returns 
   */
  async get(config) {
    // Get list of all unique packages
    let packages = [];
    config.forEach((entry) => {
      packages = [
        ...packages,
        ...this.getUniquePackages(entry.packages, packages),
      ];
    });
    // Get compiled source code from all packages
    const components = {};
    await Promise.all(packages.map(async (packageEntry) => {
      const code = await getSourceCodeFromUrl(`${packageEntry.host}server.js`);
      const result = vm.runInNewContext(code, { React, isSSR: true });
      components[packageEntry.name] = result;
    }));
    return components;
  }
}

export default Codes;