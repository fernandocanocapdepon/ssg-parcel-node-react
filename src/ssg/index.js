
import fetch from 'node-fetch';
import { argv } from 'process';
import Translations from './translations';
import Builder from './builder';
import Codes from './codes';
import { unique } from './helpers';
/**
 * Build a statics html from a json configuration
 */
new (class {
  constructor() {
    this.builder = new Builder();
  }
  /**
   * @param {string} jsonConfigUrl url to config json file
   */
  async main(jsonConfigUrl) {
    const codes = new Codes();
    const config = await fetch(jsonConfigUrl).then((res) => res.json());
    const packagesCodes = await codes.get(config);
    const translations = new Translations(
      'http://localhost:8000',
      config.map((entry) => entry.locales).filter(unique)
    );
    // Get all translations of all templates in all locales requested
    const translationsResult = await translations.translationsFromTemplates(
      config.map((entry) => entry.template),
    );
    // Generate all config requested templates
    await Promise.all(config.map(async (entry) => {
      return await Promise.all(entry.locales.map(async (locale) => {
        return await this.builder.entry(entry, locale, translationsResult, packagesCodes)
      }))
    }));
  }
})().main(argv.slice(2)[0]);
