/**
 * Channel to send messages
 */
class Channel {
  constructor() {
    this.listener = null;
  }

  /**
   * Send message to the listener
   * @param {Object} message 
   */
  send(message) {
    if (this.listener) {
      this.listener(message);
    }
  }

  /**
   * Stop to listen messages
   */
  clear() {
    this.listener = null;
  }

  /**
   * Define a listener to get then channel messages
   * @param {*} listener 
   */
  messageListener(listener) {
    if (!this.listener) {
      this.listener = listener;
    }
  }
}

/**
 * To be able to send messages between micro-frontends
 */
class Channels {

  constructor() {
    this.channels = {};
  }

  /**
   * Creates a new channel to send messages
   * @param {string} channelId 
   * @returns 
   */
  createChannel(channelId) {
    if (channelId) {
      this.channels[channelId] = new Channel()
      return this.channels[channelId];
    }
    return null;
  }

  /**
   * Send message to a channel
   * @param {string} channelId 
   * @param {Object} message 
   */
  sendMessage(channelId, message) {
    if (this.channels[channelId]) {
      this.channels[channelId].send(message);
    }
  }
}

export default Channels;


/**
  ### How to create a channel
  ```
  window.verne.channels.createChannel('myCustomChannel')
      .messageListener((message) => {
        console.log('I recibe this message:', message);
      });
  ```
   ### How to send a message
  ```
  window.verne.channels.sendMessage('myCustomChannel',{ myData: 'Hello, I am send this message'});
  ```
  */