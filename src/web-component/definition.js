import WebComponent, { jsx } from "./webComponent";
/** @jsx jsx */

/**
 * Web-component to load micro-frontend definition files (js, css...)
 */
WebComponent("package-definition",
  /**
   * 
   * @typedef {Object} MicroDefinitionProps
   * @property {import("./webComponent").WebComponentType} el web-component element
   * @property {string} name web-component path name
   * @property {string} host web-component host url
   * @param {MicroDefinitionProps} microDefinitionProps 
   */
  (packageDefinitionProps) => {
    const { el, name, host = '' } = packageDefinitionProps;
    /**
     * Load all instances of a micro-frontend
     */
    const loadInstances = () => {
      document.querySelectorAll(`[package-def="${name}"]`).forEach((container) => {
        ACEROLA.load(name, container, { ...container.attributes, ...el.props });
      })
    }
    /**
     * Load micro-frontend scripts files
     * @param {string} mainUrl
     * @param {Function} next continues with main flow
     */
    const renderScript = (mainUrl, next) => {
      const script = document.createElement("script");
      script.src = mainUrl;
      script.onload = () => {
        next();
      };
      document.body.appendChild(script);
    };
    /**
     * Render micro-frontend  styles files
     * @param {string} stylesLtr left to right styles
     * @param {string} stylesRtl right to left styles 
     */
    const renderStyle = (stylesLtr, stylesRtl) => {
      const link = document.createElement("link");
      link.rel = "stylesheet";
      link.href = document.documentElement.dir === 'rtl' ? stylesRtl : stylesLtr;
      document.head.appendChild(link);
    };
    /**
     * 
     * @param {Object} manifest json with name of files
     * @param {Object} params params to load micro-frontend
     * @param {Function} next continues with main flow
     */
    const renderFiles = (manifest, next) => {
      renderStyle(
        `${host}${name}${manifest["styles.css"]}`,
        `${host}${name}${manifest["styles.rtl.css"]}`
      );
      renderScript(
        `${host}${name}${manifest["main.js"]}`,
        next,
      );
    }
    /**
     * Load definition of a micro-frontend
     */
    const loadDefinition = () => {
      const hasInstances = document.querySelectorAll(`[package-def="${name}"]`);
      if (hasInstances && hasInstances.length) {
        const manifestUrl = `${host}${name}/manifest.json`;
        try {
          // Get manifest of micro-frontend
          fetch(manifestUrl)
            .then((response) => response.json())
            .then((manifest) => {
              renderFiles(
                manifest,
                () => loadInstances(),
              );
            });
        } catch (ex) {
          console.error(`[Manguito] Fail loading ${name}`, ex);
        }
      }
    };
    el.componentDidLoaded = () => loadDefinition();
  })