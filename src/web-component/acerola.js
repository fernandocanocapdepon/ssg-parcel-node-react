import Translator from './translator';
import Channels from './channels';

class Acerola {
  constructor() {
    this.instances = 0;
    this.definitions = {};
    this.translations = new Translator();
    this.channels = new Channels();
  }
  subscribe(def, callback) {
    this.definitions[def] = callback;
    const count = document.querySelectorAll(`[package-def="${def}"]`).length;
    this.instances += count;
  }
  load(def, el, props) {
    const next = () => {
      this.instances -= 1;
      if (this.instances === 0) {
        console.log('all loaded');
      }
    };
    this.definitions[def](el, props, next);
  }
}

window.ACEROLA = new Acerola();