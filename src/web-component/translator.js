class Translator {
  constructor() {
    this.cache = {};
    this.subscriptions = [];
    this.pendingKeys = [];
  }

  /**
   * Gets a list of translations from cache
   * @param {String[]} keys target keys to be obtained from cache
   * @returns {{key: {lang: value}}[]}
   */
  getTranslationsFromCache(keys) {
    const translations = {};
    keys.forEach((key) => { translations[key] = this.cache[key]; });
    return translations;
  }

  /**
   * Gets translations from getLabels service and stores in cache
   */
  process() {
    const language = document.documentElement?.lang || navigator.language?.split('-')[0];
    fetch(`http://localhost:8000/ws-labels?idioma=${language}`, {
      method: 'POST',
      body: JSON.stringify({ keys: this.pendingKeys }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then((res) => res.json())
    .then((result) => {
      this.cache = { ...this.cache, ...result };
      this.resolveKeys();
    })
    .catch(() => this.rejectAll());
  }

  /**
   * Fallback function when the process fails
   */
  rejectAll() {
    this.subscriptions.forEach((subscription) => {
      subscription.reject(new Error('Translations service error'));
    });
    this.subscriptions = [];
    this.pendingKeys = [];
  }

  /**
   * Obtain all translations for each subscription
   */
  resolveKeys() {
    this.subscriptions.forEach((subscription) => {
      subscription.resolve(this.getTranslationsFromCache(subscription.keys));
    });
    this.subscriptions = [];
    this.pendingKeys = [];
  }

  /**
   * Requests a list of keys
   * @param {String[]} keys array with translations keys requested
   */
  subscribe(keys) {
    const that = this;
    const resolved = new Promise((resolve, reject) => {
      const newKeys = keys.filter((key) => !Object.keys(that.cache).includes(key));

      if (newKeys.length) {
        if (!that.pendingKeys.length) {
          setTimeout(that.process.bind(that));
        }

        const isUniqueKey = (value, index, self) => self.indexOf(value) === index;
        that.pendingKeys = [...that.pendingKeys, ...newKeys].filter(isUniqueKey);
        that.subscriptions.push({ keys, resolve, reject });
      } else {
        resolve(that.getTranslationsFromCache(keys));
      }
    });
    return resolved;
  }
}

export default Translator;