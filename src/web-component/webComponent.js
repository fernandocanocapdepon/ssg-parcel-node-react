const ELEMENT_PREFIX = 'element: ';

class WebComponentClass extends window.HTMLElement {
  constructor() {
    super();
    this.props = {};
    this.listeners = [];
    this.customEvents = this.customEvents || {};
  }

  /**
   * Create attributes observers to update the component
   * when changes will detected
   */
  observableWebComponent() {
    const observer = new window.MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.type === 'attributes') {
          const newValue = mutation.target.getAttribute(mutation.attributeName);
          this.processAttr(mutation.attributeName, newValue);
          this.renderWebComponent();
          this.componentDidUpdated(this);
        }
      });
    });
    observer.observe(this, {
      attributes: true,
    });
  }

  /**
   * Convert a virtual node to a HTML node
   * @param {Object} virtualNode
   */
  vdom(virtualNode) {
    if (virtualNode) {
      if (typeof virtualNode === 'string') return document.createTextNode(virtualNode);
      const currentNode = document.createElement(virtualNode.nodeName);
      Object.keys(virtualNode.attributes || {}).forEach((attributeName) => {
        if (attributeName.startsWith('on') && !attributeName.startsWith('on-')) {
          this.listeners.push(
            this.addEventListener(
              attributeName.replace('on', '').toLowerCase(),
              currentNode,
              virtualNode.attributes[attributeName],
            ),
          );
        } else if (attributeName.startsWith('on-')) {
          currentNode
            .customEvents[this.toCamel(attributeName)] = virtualNode.attributes[attributeName];
          currentNode.setAttribute(attributeName, virtualNode.attributes[attributeName]);
        } else {
          currentNode.setAttribute(attributeName, virtualNode.attributes[attributeName]);
        }
      });
      (virtualNode.children || []).forEach((childNode) => {
        const child = this.vdom(childNode);
        if (child) {
          currentNode.appendChild(child);
        }
      });
      return currentNode;
    }
    return null;
  }

  /**
   * Parse attributes to json objects
   * @param {Object[]} propsParam
   * @param {string} attr
   * @param {string} value
   */
  static parseProperty(propsParam, attr, value) {
    const props = propsParam;
    props[attr] = value;
    if (value === 'true' || value === 'false') {
      props[attr] = !!(value && JSON.parse(value));
    } else {
      try {
        props[attr] = (value && JSON.parse(value));
      } catch (ex) {
        return props;
      }
    }
    return props;
  }

  /**
   * Render web-component, style and html
   */
  renderWebComponent() {
    const style = document.createElement('style');
    const template = document.createElement('template');
    this.innerHTML = '';
    const jsxResult = this.render(this.props);
    if (jsxResult) {
      template.content.appendChild(this.vdom(jsxResult));
    }
    const cssModules = this.styleRules();
    if (cssModules) {
      Object.keys(cssModules).forEach((rule) => {
        const ruleStyles = cssModules[rule];
        if (rule.startsWith(ELEMENT_PREFIX)) {
          const target = template.content.querySelector(
            rule.replace(ELEMENT_PREFIX, ''),
          );
          if (target) {
            Object.keys(ruleStyles).forEach((cssProperty) => {
              target.style[cssProperty] = ruleStyles[cssProperty];
            });
          }
        } else {
          style.innerHTML += `${rule} {\n`;
          const cssText = Object.keys(ruleStyles)
            .map((cssProperty) => `\t${WebComponentClass.toDash(cssProperty)}: ${ruleStyles[cssProperty]};\n`)
            .join('');
          style.innerHTML += cssText;
          style.innerHTML += '}\n';
        }
      });
    }
    if (this.hasAttribute('shadow')) {
      let shadow = this.shadowRoot;
      if (!this.shadowRoot) {
        shadow = this.attachShadow({ mode: 'open' });
      }
      shadow.innerHTML = '';
      if (cssModules) {
        shadow.appendChild(style);
      }
      shadow.appendChild(template.content);
    } else {
      if (cssModules) {
        this.appendChild(style);
      }
      this.appendChild(template.content);
    }
  }

  /**
   * Creates a vdom object (jsx) from html
   * @param {*} children current children
   * @param {*} list initial children list
   * @returns
   */
  reverseVirtualDom(children, list) {
    children.forEach((child) => {
      if (child instanceof window.Text) {
        list.push(child.data);
      } else {
        let childNodes;
        if (child.childNodes) {
          childNodes = this.reverseVirtualDom(child.childNodes, []);
        }
        list.push({
          nodeName: child.nodeName,
          attributes: child.attributes,
          children: childNodes,
        });
      }
    });
    return list;
  }

  /**
   * Create a new event listener for a event
   * @param {*} eventName
   * @param {*} element element where listening
   * @param {*} callback function to execute on event occurs
   * @returns
   */
  addEventListener(eventName, element, callback) {
    const ref = (e) => {
      callback(this.props, e);
    };
    this.props[eventName] = () => {
      // eslint-disable-next-line no-eval
      window.eval(ref);
    };
    if (element) {
      element.addEventListener(eventName, ref);
      return () => {
        element.removeEventListener(eventName, ref);
      };
    }
    return null;
  }

  /**
   * To dashed format converter
   * @param {*} str to convert
   * @returns {string}
   */
  static toDash(str) {
    return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
  }

  /**
   * To camel-case format converter
   * @param {*} str to convert
   * @returns {string}
   */
  static toCamel(str) {
    return str.replace(/[-_]([a-z])/g, (g) => g[1].toUpperCase());
  }

  processAttr(attributeName, newValue) {
    this.getAttributeNames().forEach((attr) => {
      if (attr.startsWith('on-')) {
        if (
          typeof this.getAttribute(attr) === 'string'
          && !this.customEvents[WebComponentClass.toCamel(attr)]
        ) {
          this.customEvents[
            WebComponentClass.toCamel(attr)
            // eslint-disable-next-line no-eval
          ] = () => window.eval(this.getAttribute(attr));
        }
      } else {
        this.props = WebComponentClass.parseProperty(
          this.props,
          WebComponentClass.toCamel(attr),
          attributeName === attr ? newValue : this.getAttribute(attr),
        );
      }
    });
  }

  /**
   * Set list of rules
   * @returns {Object} list of rules with css properties
   */
  // eslint-disable-next-line class-methods-use-this
  styleRules() { return {}; }

  /**
   * Occurs when component was loaded first time
   */
  componentDidLoaded() { return this.props; }

  /**
   * Occurs when component was updated by attribute change
   */
  componentDidUpdated() { return this.props; }

  /**
   * Occurs when component was unmounted
   */
  componentDidUnmounted() { return this.props; }

  /**
   * Re-render components adding state
   * @param {Object} state re-render components adding this properties
   */
  renderWith(state) {
    this.props = {
      ...state,
      ...this.props,
    };
    this.renderWebComponent();
  }

  /** */
  disconnectedCallback() {
    this.listeners.forEach((listenerRemove) => {
      listenerRemove();
    });
    this.componentDidUnmounted(this);
  }

  /** */
  connectedCallback() {
    setTimeout(() => {
      this.props = {};
      if (this.hasAttribute('observable')) {
        this.observableWebComponent();
      }
      this.processAttr();
      if (this.customEvents) {
        Object.keys(this.customEvents).forEach((customEvent) => {
          this.props[customEvent] = this.customEvents[customEvent];
        });
      }
      this.props.children = this.reverseVirtualDom(this.cloneNode(true).childNodes, []);
      this.props.el = this;
      this.renderWebComponent();
      window.dispatchEvent(new window.CustomEvent(`${this.tagName}Loaded`));
      this.componentDidLoaded();
    });
  }
}
export const jsx = (nodeName, attributes, ...args) => {
  const children = args.length ? [].concat(...args) : null;
  return {
    nodeName,
    attributes,
    children
  };
};
/**
 * @typedef {Object} WebComponentType
 * @property {Function} componentDidLoaded
 * @property {Function} componentDidUpdated
 * @property {Function} componentDidUnmounted
 * @property {Object} props
 * @property {Function} renderWith
 * @property {Function} styleRules
 *
 * @typedef {Object} WebComponentParams
 * @property {WebComponentType} el
 *
 * @callback WebComponentCallback
 * @param {WebComponentParams} param
 */
/**
 * @param {string} tag
 * @param {WebComponentCallback} renderCallback
 */
const WebComponent = (tag, renderCallback) => {
  class ClassExtended extends WebComponentClass { }
  ClassExtended.prototype.render = renderCallback;
  window.customElements.get(tag) ||
    window.customElements.define(tag, ClassExtended);
};

export default WebComponent;
