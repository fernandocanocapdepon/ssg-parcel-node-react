import resolve from '@rollup/plugin-node-resolve';
import builtins from 'rollup-plugin-node-builtins';
import commonjs from '@rollup/plugin-commonjs';
import babel from 'rollup-plugin-babel';
import scss from 'rollup-plugin-scss';
import replace from 'rollup-plugin-replace';
import minify from 'rollup-plugin-babel-minify';

export default () => {
  const plugins = [
    resolve({
      preferBuiltins: true,
      browser: true,
      extensions: ['.js', '.jsx', '.json'],
    }),
    builtins(),
    babel({
      exclude: 'node_modules/**',
      runtimeHelpers: true,
    }),
    commonjs(),
    minify(),
    replace({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    scss({
      output: './styles.css',
      outputStyle: 'compressed',
    })
  ];
  return [
    {
      input: './src/main.jsx',
      external: ['react', 'react-dom'],
      output: [
        { file: './main.js', format: 'iife' },
      ],
      plugins,
    },
    {
      input: './src/server.jsx',
      external: ['react', 'react-dom'],
      output: [
        { file: './server.js', format: 'iife' },
      ],
      plugins,
    },
    {
      input: './src/hydrate.jsx',
      external: ['react', 'react-dom'],
      output: [
        { file: './hydrate.js', format: 'iife' },
      ],
      plugins,
    }
  ];
}