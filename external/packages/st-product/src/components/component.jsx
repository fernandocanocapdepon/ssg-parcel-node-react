import React, { useEffect, useState } from 'react';
import './component.scss';

const Product = ({ defaultProduct, defaultTranslations }) => {
  const [translations, setTranslations] = useState(defaultTranslations);
  const [product, setProduct] = useState(defaultProduct);
  const getTranslations = async () => {
    const result = await window.ACEROLA.translations.subscribe(['header']);
    setTranslations(result);
  }
  const getProduct = async () => {
    const id = document.location.pathname.replace('/product/', '');
    const result = await fetch(`http://localhost:8000/ws-product/${id}`).then((res) => res.json());
    setProduct(result);
  }
  useEffect(() => {
    if (!translations) {
      getTranslations();
    }
    if (!product) {
      getProduct()
    }
  }, []);

  return product && translations ? (
    <div className="st-product" onClick={() => {
      ACEROLA.channels.sendMessage('myHeaderChannel','Hello header, I am a product');
    }}>
      <img src={`${product.imageUrl}?imwidth=192&imdensity=1`} />
      <p>{product.name}</p>
      <p>{product.price}</p>
      <p>{translations.header.es}</p>
    </div>
  ) : null;
};

export default Product;