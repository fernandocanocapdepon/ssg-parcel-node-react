import React from 'react';
import ReactDOM from 'react-dom';
import Product from './components/component';

const els = document.querySelectorAll('[hydrate="st-product"]');
if (els) {
  els.forEach((el) => {
    ReactDOM.hydrate(<Product {...window.__hydrated['st-product']} />, el);
  });
}