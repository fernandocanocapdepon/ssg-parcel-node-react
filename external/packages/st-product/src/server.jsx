import Product from './components/component';

export const getStatics = async (fetch, { productId }) => {
  const defaultTranslations = await fetch('http://localhost:8000/ws-labels', {
    method: 'POST',
    body: JSON.stringify({ keys: ['header'] }),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((res) => res.json());
  const defaultProduct = await fetch(`http://localhost:8000/ws-product/${productId}`)
    .then(res => res.json());
  return new Promise((resolve) => {
    resolve({
      defaultProduct,
      defaultTranslations,
    });
  });
}
export default Product;