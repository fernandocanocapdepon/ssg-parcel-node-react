import React from 'react';
import ReactDOM from 'react-dom';
import Footer from './components/component';

const els = document.querySelectorAll('[hydrate="st-footer"]');
if (els) {
  els.forEach((el) => {
    ReactDOM.hydrate(<Footer {...window.__hydrated['st-footer']} />, el);
  });
}