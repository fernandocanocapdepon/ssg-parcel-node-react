import React from 'react';
import './component.scss';

const Footer = () => (
  <nav className="footer">
    <ul>
      <li><a href="/home">HOME</a></li>
      <li><a href="/catalog">Mujer</a></li>
      <li><a href="/catalog">Hombre</a></li>
    </ul>
  </nav>
);

export default Footer;