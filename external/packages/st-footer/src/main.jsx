import React from 'react';
import ReactDOM from 'react-dom';
import Footer from './components/component';

ACEROLA.subscribe('st-footer',(el, props, next) => {
  console.log(props);
  ReactDOM.render(<Footer />, el);
  next();
});