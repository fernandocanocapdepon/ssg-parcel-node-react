import React, { useEffect, useState } from 'react';
import './component.scss';

import PathMatch from '../helpers/path-match';

const Catalog = ({ defaultCatalog }) => {
  const [catalog, setCatalog] = useState(defaultCatalog);
  const fetchData = () => {
    const pathMatch = PathMatch('/catalog/:label');
    const params = pathMatch.match(document.location.pathname);
    fetch(`http://localhost:8000/ws-catalog/${params.label ? params.label : ''}`)
      .then(res => res.json())
      .then(async (responseData) => {
        console.log(responseData);
        setCatalog(responseData.products);
      })
  }

  useEffect(() => {
    if (!catalog) {
      fetchData();
    }
  }, []);

  return catalog ? (
    <ul className="st-catalog">
      {catalog.map((product) => (
        <div key={product.productId} className="product" onClick={() => console.log('image click')}>
          <a href={`/product/${product.productId}`}>
            <img src={`${product.imageUrl}`} />
          </a>
          {product.isMock ? (
            <>
              <p>...</p>
              <p>...</p>
            </>
          ) : (
            <>
              <p>{product.name}</p>
              <p>{product.price}</p>
            </>
          )}
        </div>
      ))
      }
    </ul >
  ) : null;
};

export default Catalog;