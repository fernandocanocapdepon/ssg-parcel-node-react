const PathMatch = (pattern) => {
  const converter = (inString) => {
    const regexp = new RegExp(/(\/[:a-zA-Z0-9_]+)/gi);
    const output = [];
    let current = regexp.exec(inString);
    while (current) {
      output.push(current[1].substring(1));
      current = regexp.exec(inString);
    }
    return output;
  };
  const matchTest = (pattern, test) => {
    const out = {};
    let found = true;
    test.forEach((element, index) => {
      if (pattern[index]) {
        if (pattern[index].startsWith(":")) {
          out[pattern[index].substring(1)] = element;
        } else if (pattern[index] !== element) {
          found = false;
        }
      } else {
        found = false;
      }
    });
    return found && out;
  };
  return {
    match(test) {
      return matchTest(converter(pattern), converter(test));
    }
  };
};

export default PathMatch;