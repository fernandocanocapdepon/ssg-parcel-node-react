import React from 'react';
import ReactDOM from 'react-dom';
import Catalog from './components/component';

const els = document.querySelectorAll('[hydrate="st-catalog"]');
if (els) {
  els.forEach((el) => {
    ReactDOM.hydrate(<Catalog {...window.__hydrated['st-catalog']} />, el);
  });
}