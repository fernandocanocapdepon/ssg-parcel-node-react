import Catalog from './components/component';

export const getStatics = async (fetch, { label }) => new Promise((resolve) => {
  fetch(`http://localhost:8000/ws-catalog/${label}`)
    .then(res => res.json())
    .then(async (responseData) => {
      console.log(responseData);
      resolve({
        defaultCatalog: responseData.products
      });
    });
});

export default Catalog;