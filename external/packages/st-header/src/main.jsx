import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/component';

ACEROLA.subscribe('st-header',(el, props, next) => {
  ReactDOM.render(<Header {...props}/>, el);
  next();
});