import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/component';

const els = document.querySelectorAll('[hydrate="st-header"]');
if (els) {
  els.forEach((el) => {
    ReactDOM.hydrate(<Header {...window.__hydrated['st-header']} />, el);
  });
}