import React, { useEffect, useState } from 'react';
import './component.scss';

const Header = ({ defaultMenu }) => {
  const [menu, setMenu] = useState(defaultMenu);
  const getMenu = async () => {
    const menuResult = await fetch('http://localhost:8000/ws-menu').then((res) => res.json());
    setMenu(menuResult);
  }
  useEffect(() => {
    if (!menu) {
      getMenu();
    }
    ACEROLA.channels.createChannel('myHeaderChannel')
      .messageListener((message) => {
        console.log('I am the header and recibe this message:', message);
      });
  })
  return menu ? (
    <nav className="header">
      <ul>
        {menu.map((item) => (
          <li key={item.href}><a href={item.href}>{item.label}</a></li>
        ))}
      </ul>
    </nav>
  ) : null;
};

export default Header;